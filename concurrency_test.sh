# setup
redis-cli -p 5379
set a 1

# proc 1
redis-cli -p 5379
multi
incr a
exec

# proc 2, déclenché au milieu de proc1
incr a
