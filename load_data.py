# pip3 install virtualenv
# virtualenv -p python3 venv
# source venv/bin/activate
# pip install pandas redis

# Load the Pandas libraries with alias 'pd' 
import pandas as pd 

data = pd.read_csv("NFA 2017 EDITION.csv") 

# Preview the first 5 lines of the loaded data 
data.head()

import redis
# connect to tidis cluster
redis_instance = redis.Redis(host='localhost', port=5379, db=0)

for index, row in data.iterrows():
    key = str(row['country_code'])+':'+str(row['year'])+':'+row['record']
    redis_instance.set(key, row['total'])

redis_instance.get('1:1992:AreaPerCap')
